## mot2 (MOC on tint2)

Python script intended for use with [Music on Console][moc] as a [tint2][tint2] executor. Formats 'mocp -i' into a single-line output, with optional scrolling and icon output.

### Screenshot

![animated!](https://i.imgur.com/i88GxlX.gif)

### Usage:

    usage: mot2 [options]

    -h, --help                          show this help message and exit  
    
    -l CHARS, --length CHARS            Max characters per line. Default 50.  
    
    -f [ALIGN], --fixed [ALIGN]         Fixed length. Adds spaces as needed with alignment. 
                                        Default: right  
    
    -t, --timer                         Appends timer.  
    
    -c [SECS], --continuous [SECS]      Run continuously. Delay default 1sec.  
    
    -i [PATH], --icon [PATH]            Output path to state icon as first line. Looks for 
                                        "[play|pause|stop].png"  in cwd by default. 
                                        ex: "/path/to/icon/theme/actions/24/media_*.svg"  

    -s, --scroll                        Scroll output longer than length. Continuous 
                                        output will be enabled.  
    
    -a, --artist                        Add artist name to output.  


### tint2 Config

Use of monospace font strongly advised for a clean appearance.

Example tint2 setup:

    # E = EXECP
    execp = new
    execp_command = mot2 -t -a -f -s -i '/home/cs/.icons/Flattr Dark/actions/24/player_*.svg'
    execp_interval = 0
    execp_continuous = 2
    execp_has_icon = 1 # icon path must be first line of output
    execp_cache_icon = 0
    execp_icon_w = 20
    execp_icon_h = 20
    execp_tooltip = 
    execp_font = Liberation Mono Regular 8
    execp_font_color = #828282 90
    execp_markup = 0 
    execp_background_id = 8
    execp_centered = 1
    execp_padding = 1 2 0
    execp_lclick_command = xdotool key Super+F2
    execp_mclick_command = mocp --next
    execp_rclick_command = mocp --toggle-pause
    execp_uwheel_command = amixer sset PCM,0 4+
    execp_dwheel_command = amixer sset PCM,0 4-

### Thanks

[ohnonot - Media Player Info][mpi]

[o9000 - tint2][tint2]

[Everyone at bunsenlabs][bl]

[tint2]: https://gitlab.com/o9000/tint2
[mpi]: https://github.com/ohnonot/media-player-info
[moc]: http://moc.daper.net/
[bl]: https://forums.bunsenlabs.org/